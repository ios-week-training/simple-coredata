//
//  CoreDataContactApp.swift
//  CoreDataContact
//
//  Created by Iqbal Rahman on 31/03/24.
//

import SwiftUI

@main
struct CoreDataContactApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
//            ContentView()
            ListView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
