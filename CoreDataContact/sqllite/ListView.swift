//
//  ListView.swift
//  CoreDataContact
//
//  Created by Iqbal Rahman on 01/04/24.
//

import SwiftUI

struct ListView: View {
    @State var allTask: [Task] = []
    let presenter = TaskDataStore.shared
    
    var body: some View {
        ZStack {
            VStack(spacing: 30) {
                Text("Welcome !!!")
                    .font(.title.bold())
                
                if allTask.isEmpty {
                    Text("You have no task.")
                    Button("fetch") {
                        allTask = presenter.getAllTasks()
                    }
                } else {
                    List {
                        ForEach(allTask, id: \.id) { task in
                            Text(task.name)
//                                .onTapGesture {
//                                    viewModel.appPilot.push(.Detail(id: task.id))
//                                }
                        }
                        .onDelete(perform: deleteTask(at:))
                    }
                    .listStyle(.plain)
                    .onAppear {
                        UITableView.appearance().backgroundColor = .clear
                        UITableViewCell.appearance().selectionStyle = .none
                        UITableView.appearance().showsVerticalScrollIndicator = false
                    }
                }
            }
        }
        .onAppear {
            _ = presenter.insert(name: "Tugas 1", date: Date())
        }
    }
    
    func deleteTask(at indexSet: IndexSet) {
        let id = indexSet.map { self.allTask[$0].id }.first
        if let id = id {
            let delete = TaskDataStore.shared.delete(id: id)
            if delete {
                allTask = presenter.getAllTasks()
            }
        }
    }
}

#Preview {
    ListView()
}
