//
//  Task.swift
//  CoreDataContact
//
//  Created by Iqbal Rahman on 01/04/24.
//

import Foundation

struct Task {
    let id: Int64
    var name: String
    var date: Date
    var status: Bool
}
