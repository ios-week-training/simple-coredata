//
//  GetAllListTest.swift
//  CoreDataContactTests
//
//  Created by Iqbal Rahman on 01/04/24.
//

import XCTest

final class GetAllListTest: XCTestCase {
    
    var allTask: [Task]!
    var presenter: TaskDataStore!

    override func setUpWithError() throws {
        allTask = []
        presenter = TaskDataStore.shared
    }

    override func tearDownWithError() throws {
        allTask = nil
    }
    
    func testInit() {
        let length = allTask.count
        XCTAssertEqual(length, 0)
    }

    func testGetAllData() throws {
        allTask = presenter.getAllTasks()
        XCTAssertEqual(allTask.count, 0)
    }

}
